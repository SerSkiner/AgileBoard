package com.agile.AgileBoard.model;

public class Cards {
    private Long CARD_ID;
    private String CARDNAME;
    private String STATUSCARD;



    public Cards(Long CARD_ID, String CARDNAME, String STATUSCARD) {
        this.CARD_ID = CARD_ID;
        this.CARDNAME = CARDNAME;
        this.STATUSCARD = STATUSCARD;
    }

    public Cards() {

    }

    public Long getCARD_ID() {
        return CARD_ID;
    }

    public void setCARD_ID(Long CARD_ID) {
        this.CARD_ID = CARD_ID;
    }

    public String getCARDNAME() {
        return CARDNAME;
    }

    public void setCARDNAME(String CARDNAME) {
        this.CARDNAME = CARDNAME;
    }

    public String getSTATUSCARD() {
        return STATUSCARD;
    }

    public void setSTATUSCARD(String STATUSCARD) {
        this.STATUSCARD = STATUSCARD;
    }
}
