package com.agile.AgileBoard.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.agile.AgileBoard.model.Cards;
import org.springframework.jdbc.core.RowMapper;

public class CardMapper implements RowMapper<Cards> {

    public static final String Base_SQL//
    = "Select ca.card_id, ca.CARDNAME, ca.STATUSCARD From Cards ca ";

    @Override
    public Cards mapRow(ResultSet resultSet, int i) throws SQLException {



        Long CARD_id = resultSet.getLong("CARD_ID");
        String CARDNAME = resultSet.getString("CARDNAME");
        String STATUSCARD = resultSet.getString("STATUSCARD");

        return new Cards(CARD_id, CARDNAME,STATUSCARD);
    }
}
