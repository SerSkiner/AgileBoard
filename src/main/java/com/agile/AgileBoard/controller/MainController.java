package com.agile.AgileBoard.controller;

import java.util.Collections;
import java.util.List;
import com.agile.AgileBoard.model.Cards;
import com.agile.AgileBoard.dao.CardsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class MainController {
    @Autowired

    private CardsDAO cardsDAO;
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showCards(Model model){
        List<Cards> list = cardsDAO.getDone();
        List<Cards> cardsDAOInProgress = cardsDAO.getInProgress();
        List<Cards> cardsDAOToDo = cardsDAO.getToDo();


        model.addAttribute("Done", list);
        model.addAttribute("InProgress", cardsDAOInProgress);
        model.addAttribute("ToDo",cardsDAOToDo);


        return "cardsPage";
    }

    @RequestMapping(value = "/greateCard", method = RequestMethod.GET)
    public String viewCardPage(Model model) {

        Cards cards = new Cards();

        model.addAttribute("greateCard", cards);

        return "greateCard";
    }

    @RequestMapping(value = "/greateCard", method = RequestMethod.POST)
    public String processSendMoney(Model model, Cards cards) {
        cardsDAO.create(cards.getCARDNAME(),cards.getSTATUSCARD());

        return "redirect:/";
    }



}
