package com.agile.AgileBoard.dao;

import java.util.List;

import javax.sql.DataSource;
import com.agile.AgileBoard.model.Cards;
import com.agile.AgileBoard.mapper.CardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class CardsDAO extends JdbcDaoSupport {

    @Autowired
    public CardsDAO(@Qualifier("dataSource") DataSource dataSource) {
        this.setDataSource(dataSource);
    }
    private JdbcTemplate jdbcTemplateObject;


    public List<Cards> getDone() {
        //"Select ca.card_id, ca.CARDNAME, ca.cardstatus From Cards ca ";
        String sql = "select * from cards where STATUSCARD LIKE 'DONE'";

        Object params = new Object[]{};
        CardMapper mapper = new CardMapper();
        List<Cards> list = this.getJdbcTemplate().query(sql, (Object[]) params, mapper);
        return list;

    }

    public List<Cards> getInProgress() {
        //"Select ca.card_id, ca.CARDNAME, ca.cardstatus From Cards ca ";
        String sql = "select * from cards where STATUSCARD LIKE 'In Progress'";

        Object params = new Object[]{};
        CardMapper mapper = new CardMapper();
        List<Cards> cards = this.getJdbcTemplate().query(sql, (Object[]) params, mapper);
        return cards;

    }

    public List<Cards> getToDo() {
        //"Select ca.card_id, ca.CARDNAME, ca.cardstatus From Cards ca ";
        String sql = "select * from cards where STATUSCARD LIKE 'To DO'";

        Object params = new Object[]{};
        CardMapper mapper = new CardMapper();
        List<Cards> cards1 = this.getJdbcTemplate().query(sql, (Object[]) params, mapper);
        return cards1;

    }

    public void create(String CARDNAME, String STATUSCARD) {
        String SQL = "insert into Cards (CARDNAME, STATUSCARD) values (?, ?)";
        Object params = new Object[]{};
        CardMapper mapper = new CardMapper();
        int cards = this.getJdbcTemplate().update(SQL,CARDNAME,STATUSCARD);
        return;
    }

    public void update(Long CARD_ID, String STATUSCARD) {
        String SQL = "UPDATE Cards SET STATUSCARD = ? WHERE CARD_ID = ?";
        Object params = new Object[]{};
        CardMapper mapper = new CardMapper();
        int cards = this.getJdbcTemplate().update(SQL,CARD_ID,STATUSCARD);
        return;
    }
}


